#COCCOC TEST PROJECT#

##Demonstration
http://vn.salamakhin.ru/topics

##Methods
Input\output data type - JSON

#### POST /topic
params: token, subject, text  
*Create topic*
#### POST /topic/{topicId}/comment
params: text  
*Add comment to topic*
#### GET /topic/{topicId}
*Get topic by id*
#### GET /topics
*Get full topics list*
#### DELETE /topic/{topicId}
*Delete topic and comments by topicId*

##Installing
Clone repo
```
cd {projectPath};
git clone git@bitbucket.org:343604/coccoc.git
cd coccoc;
```

Composer installing
```
composer install
```

Setup config
```
cp config/config.dist.php config/config.php;
vi config/config.php;
```

Create DB structure and add test user
```
mysql -p -u {user} {dbname} < db.sql;
```

Configure web-server entry point
```
web/app.php
```