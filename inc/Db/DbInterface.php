<?php
namespace Api\Db;

/**
 * Database Controller Interface
 */
interface DbInterface
{
    /**
     * Constructor with connection to db
     *
     * @param array $params (host,name,user,pass)
     *
     * @throw \Exception (Cannot connect to db)
     *
     * @return void
     */
    public function __construct($params);
  
    /**
     * Request
     *
     * @param string $query
     * @param array $params
     *
     * @throw \Exception (Cannot execute request)
     *
     * @return Statement
     */
    public function request($query, $params = []);
  
    /**
     * Fetch row to object
     *
     * @param Statement $sth
     * @param string $className
     *
     * @return object
     */
    public function fetchObject($sth, $className = 'stdClass');
  
    /**
     * Fetch row to associative array
     *
     * @param Statement $sth
     *
     * @return array
     */
    public function fetchArray($sth);
  
    /**
     * Get id by last inserted
     *
     * @return integer
     */
    public function lastInsertId();
  
    /**
     * Start transaction
     *
     * @throw \Exception (Cannot start transaction)
     *
     * @return void
     */
    public function transaction();
  
    /**
     * Commit
     *
     * @throw \Exception (Cannot commit)
     *
     * @return void
     */
    public function commit();
  
    /**
     * Rollback
     *
     * @throw \Exception (Cannot rollback)
     *
     * @return void
     */
    public function rollback();
}
