<?php
namespace Api\Db;

/**
 * PDO Adapter
 */
class DbPdo implements DbInterface
{
    private $dbh;
  
    /**
     * Constructor with connection to db
     *
     * @param array $params (host,name,user,pass)
     *
     * @throw \Exception (Cannot connect to db)
     *
     * @return void
     */
    public function __construct($params)
    {
        $dsn = sprintf('mysql:host=%s;dbname=%s', $params['host'], $params['name']);
        try {
            $this->dbh = new \PDO($dsn, $params['user'], $params['pass']);
        } catch (\PDOException $e) {
            throw new \Exception('Cannot connect to db');
        }
    }
  
    /**
     * Request
     *
     * @param string $query
     * @param array $params
     *
     * @throw \Exception (Cannot execute request)
     *
     * @return Statement
     */
    public function request($query, $params = [])
    {
        $sth = $this->dbh->prepare($query);
        if (!$sth) {
            throw new \Exception('Cannot prepare request');
        }
        if (!$sth->execute($params)) {
            throw new \Exception('Cannot execute request');
        }
        return $sth;
    }
  
    /**
     * Fetch row to object
     *
     * @param Statement $sth
     * @param string $className
     *
     * @return object
     */
    public function fetchObject($sth, $className = 'stdClass')
    {
        return $sth->fetchObject($className);
    }
  
    /**
     * Fetch row to associative array
     *
     * @param Statement $sth
     *
     * @return array
     */
    public function fetchArray($sth)
    {
        return $sth->fetch(\PDO::FETCH_ASSOC);
    }
  
    /**
     * Get id by last inserted
     *
     * @return integer
     */
    public function lastInsertId()
    {
        return $this->dbh->lastInsertId();
    }
  
    /**
     * Start transaction
     *
     * @throw \Exception (Cannot start transaction)
     *
     * @return void
     */
    public function transaction()
    {
        if (!$this->dbh->beginTransaction()) {
            throw new \Exception('Cannot start transaction');
        }
    }

    /**
     * Commit
     *
     * @throw \Exception (Cannot commit)
     *
     * @return void
     */
    public function commit()
    {
        if (!$this->dbh->commit()) {
            throw new \Exception('Cannot commit');
        }
    }
  
    /**
     * Rollback
     *
     * @throw \Exception (Cannot rollback)
     *
     * @return void
     */
    public function rollback()
    {
        if (!$this->dbh->rollBack()) {
            throw new \Exception('Cannot rollback');
        }
    }
}
