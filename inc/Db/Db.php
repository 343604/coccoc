<?php
namespace Api\Db;

use Api\Service;

/**
 * Singleton class for getting connection to db
 */
class Db
{
    private static $connection;

    /**
     * Make the constructor private.
     *
     * @return void
     */
    private function __construct()
    {
    }
  
    /**
     * Getting connection
     *
     * @return by DbInterface
     */
    public static function getConnection(Service $service)
    {
        if (!self::$connection) {
            if ($service->config['db']['type'] == 'mysql') {
                self::$connection = new DbPdo($service->config['db']);
            } else {
                throw new \Exception('Unknow db type');
            }
        }
        return self::$connection;
    }

    /**
     * Cloning prohibited
     *
     * @return void
     */
    protected function __clone()
    {
    }
}
