<?php
namespace Api\RequestHydrator;

/**
 * InvalidDataException
 */
class InvalidDataException extends \Exception
{
    private $data;
    public function __construct($data)
    {
        $this->data = $data;
        parent::__construct();
    }
    public function getData()
    {
        return $this->data;
    }
}
