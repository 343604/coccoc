<?php
namespace Api\RequestHydrator;

/**
 * AccessDenyException
 */
class NotAuthorizedException extends \Exception
{
}
