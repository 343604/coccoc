<?php
namespace Api\RequestHydrator;

use Api\Http\Request;
use Api\Model\Topic;
use Api\Model\User;
use Api\Model\Comment;

/**
 * Topic Hydrator
 */
class TopicRequestHydrator extends RequestHydrator
{
    /**
     * Topic Create Hydrate
     *
     * @param Request
     *
     * @throw InvalidDataException (Validation error)
     * @throw NotAuthorizedException (User not found)
     *
     * @return Topic
     */
    public function hydrateCreate(Request $request)
    {
        $token = (string)$request->get('token', '');
        $subject = (string)$request->get('subject', '');
        $text = (string)$request->get('text', '');
    
        $user = $this->service->getRepository('User')->findByToken($token);
        if (!$user) {
            throw new NotAuthorizedException();
        }
    
        $errors = [];
        if ($subject == '') {
            $errors['subject'] = 'Empty subject';
        }
        if ($text == '') {
            $errors['text'] = 'Empty text';
        }
        if ($errors) {
            throw new InvalidDataException($errors);
        }
        $topic = new topic();
        $topic
            ->setSubject($subject)
            ->setText($text)
            ->setUserId($user->getId());
    
        return $topic;
    }
    /**
     * Topic Hydrate
     *
     * @param Request
     *
     * @throw NotFoundException (topic not found)
     * @throw InvalidDataException (Validation error)
     *
     * @return Comment
     */
    public function hydrateAddComment(Request $request)
    {
        $text = (string)$request->get('text', '');
        $id = $request->params[0];
        
        $topic = $this->service->getRepository('Topic')->findById($id);
        if (!$topic) {
            throw new NotFoundException();
        }
    
        $errors = [];
        if ($text == '') {
            $errors['text'] = 'Empty text';
        }
        if ($errors) {
            throw new InvalidDataException($errors);
        }
    
        $comment = new Comment();
        $comment
            ->setText($text)
            ->setTopicId($topic->getId());
        
        return $comment;
    }
    /**
     * Topic Hydrate
     *
     * @param Request
     *
     * @throw AccessDenyException (Forbidden)
     * @throw NotAuthorizedException (User not found)
     * @throw NotFoundException (topic not found)
     *
     * @return Topic
     */
    public function hydrateDelete(Request $request)
    {
        $token = (string)$request->get('token', '');
        $id = $request->params[0];
    
        $user = $this->service->getRepository('User')->findByToken($token);
        if (!$user) {
            throw new NotAuthorizedException();
        }
        
        $topic = $this->service->getRepository('Topic')->findById($id);
        if (!$topic) {
            throw new NotFoundException();
        }
    
        if ($topic->getUserId() != $user->getId()) {
            throw new AccessDenyException();
        }
        
        return $topic;
    }
}
