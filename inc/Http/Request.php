<?php
namespace Api\Http;

/**
 * HTTP input request
 */
class Request
{
    private $input;
    public $params;
    public $server;
    public $action;
  
    /**
     * Handing input data
     *
     * @return void
     */
    public function __construct($server, $get, $post)
    {
        $this->server = $server;
        $this->input = json_decode(file_get_contents('php://input'), true);
    }
  
    /**
     * Get input param
     *
     * @return mixed
     */
    public function get($key, $default = null)
    {
        return isset($this->input[$key]) ? $this->input[$key] : $default;
    }
}
