<?php
namespace Api\Http;

/**
 * HTTP Response
 */
class Response
{
    public $content;

    /**
     * Set response code
     *
     * @param integer $code
     *
     * @return  Response
     */
    public function setStatusCode(int $code) : Response
    {
        http_response_code($code);
        return $this;
    }

    /**
     * Set body
     *
     * @param array $data
     *
     * @return Response
     */
    public function setJsonBody(array $data) : Response
    {
        $this->content = json_encode($data);
        $this->setHeader('Content-Type: application/json');
        return $this;
    }

    /**
     * Set body
     *
     * @param string $content
     *
     * @return Response
     */
    public function setBody(string $content) : Response
    {
        $this->content = $content;
        return $this;
    }
  
    /**
     * Add header
     *
     * @param string $header
     *
     * @return void
     */
    private function setHeader(string $header)
    {
        header($header);
    }
}
