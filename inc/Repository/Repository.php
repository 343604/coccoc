<?php
namespace Api\Repository;

use Api\Service;

/**
 * Repository class
 */
class Repository
{
    protected $service;
    public function __construct(Service $service)
    {
        $this->service = $service;
    }
}
