<?php
namespace Api\Repository;

use Api\Db\Db;

/**
 * User repository
 */
class UserRepository extends Repository
{
    /**
     * Save Topic
     *
     * @return void
     */
    public function findByToken($token)
    {
        $db = Db::getConnection($this->service);
        $request = $db->request("
            SELECT *
            FROM users
            WHERE token = ?
        ", [$token]);
    
        $User = $db->fetchObject($request, 'Api\Model\User');
    
        return $User;
    }
}
