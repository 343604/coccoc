<?php
namespace Api\Repository;

use Api\Db\Db;
use Api\Model\Topic;

/**
 * Topic repository
 */
class TopicRepository extends Repository
{
    /**
     * Save Topic
     *
     * @return void
     */
    public function save(Topic $topic)
    {
        $subject = substr($topic->getSubject(), 0, 255);
        $text = substr($topic->getText(), 0, 64000);
        $db = Db::getConnection($this->service);
        $db->request("
            INSERT INTO topics
                (subject,text,userId)
            VALUES
                (?,?,?)
        ", [$subject, $text, $topic->getUserId()]);
        $topic->setId($db->lastInsertId());
    }
  
    /**
     * Delete Topic and comments
     *
     * @throw \Exception from Db adapter
     *
     * @return void
     */
    public function delete(Topic $topic)
    {
        $db = Db::getConnection($this->service);
        $db->transaction();
        try {
            $db->request("
                DELETE
                FROM comments
                WHERE topicId = ?
            ", [$topic->getId()]);
          
            $db->request("
                DELETE
                FROM topics
                WHERE id = ?
            ", [$topic->getId()]);
          
          
            $db->commit();
        } catch (\Exception $e) {
            $db->rollback();
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * Find Topic by id
     *
     * @param integer $id
     *
     * @return object Topic
     */
    public function findById(int $id)
    {
        $db = Db::getConnection($this->service);
        $request = $db->request("
            SELECT *
            FROM topics
            WHERE id = ?
        ", [$id]);
    
        $topic = $db->fetchObject($request, 'Api\Model\Topic');
        if ($topic) {
            $commentRepository = $this->service->getRepository('Comment');
            $comments = $commentRepository->findByTopic($topic);
            $topic->setComments($comments);
        }
        
        return $topic;
    }

    /**
     * Get full topics list
     *
     * @return array
     */
    public function getList()
    {
        $db = Db::getConnection($this->service);
        $request = $db->request("
            SELECT id,subject
            FROM topics
        ");
    
        $topics = [];
        while ($row = $db->fetchArray($request)) {
            $topics[] = $row;
        }
    
        return $topics;
    }
}
