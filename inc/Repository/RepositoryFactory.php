<?php
namespace Api\Repository;

use Api\Service;

/**
 * Repository Factory
 */
class RepositoryFactory
{
    /**
     * Find repository
     *
     * @param Service
     * @param string $modelName
     *
     * @return Repository
     */
    public static function getRepository(Service $service, string $modelName)
    {
        $modelNames = ['Error', 'Comment', 'Topic', 'User'];
        if (!in_array($modelName, $modelNames)) {
            throw new \Exception('Unknow repository' . $modelName);
        }
        $classRepository = 'Api\\Repository\\' . $modelName . 'Repository';
        $repository = new $classRepository($service);

        return $repository;
    }
}
