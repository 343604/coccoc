<?php
namespace Api\Repository;

use Api\Db\Db;
use Api\Model\Topic;
use Api\Model\Comment;

/**
 * Topic comment repository
 */
class CommentRepository extends Repository
{
    /**
     * Create Comment
     *
     * @return void
     */
    public function save(Comment $comment)
    {
        $db = Db::getConnection($this->service);
        $db->request("
            INSERT INTO comments
                (text,topicId)
            VALUES
                (?,?)
        ", [$comment->getText(), $comment->getTopicId()]);
        $comment->setId($db->lastInsertId());
    }
    
    /**
     * Find comments by topic
     *
     * @param Topic $topic
     *
     * @return array Api\Model\Comment
     */
    public function findByTopic(Topic $topic)
    {
        $db = Db::getConnection($this->service);
        $request = $db->request("
            SELECT *
            FROM comments
            WHERE topicId = ?
        ", [$topic->getId()]);
    
        $comments = [];
        while ($comment = $db->fetchObject($request, 'Api\Model\Comment')) {
            $comments[] = $comment;
        }
    
        return $comments;
    }
}
