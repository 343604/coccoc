<?php
namespace Api;

use Api\Http\Request;
use Api\Http\Response;
use Api\Repository\RepositoryFactory;
use Api\Controller\ControllerFactory;

/**
 * Main Api-Service singleton class
 */
class Service
{
    /**
     * @var array $config
     */
    public $config;
    /**
     * @var Request
     */
    public $request;
    /**
     * @var Response
     */
    public $response;
  
    /**
     * Constructor
     * Creating request & response objects
     *
     * @param array $server
     * @param array $get
     * @param array $post
     *
     * @return void
     */
    public function __construct($server, $get, $post)
    {
        $this->request = new Request($server, $get, $post);
        $this->response = new Response();
    }

    /**
     * Run
     * Including config, finding controller, making and output response, exit
     *
     * @TODO logs
     */
    public function run()
    {
        try {
            if (!file_exists('../config/config.php')) {
                throw new \Exception('Config file is missing');
            }
            $this->config = include '../config/config.php';
            
            $controller = ControllerFactory::getController($this);
            $method = $this->request->action;
            $controller->$method($this->request, $this->response);
        } catch (\Exception $e) {
            //@TODO logs
            //echo $e->getMessage();
            $this->response->setStatusCode(500);
        }
        
        exit($this->response->content);
    }
  
    /**
     * Get repository.
     *
     * @param string $modelName
     *
     * @return Repository
     */
    public function getRepository(string $modelName)
    {
        return RepositoryFactory::getRepository($this, $modelName);
    }
}
