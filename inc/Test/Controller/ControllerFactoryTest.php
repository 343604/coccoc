<?php
namespace Api\Test\Controller;

use Api\Controller\ControllerFactory;

/**
 * Test Controller Factory
 */
class ControllerFactoryTest extends \PHPUnit_Framework_TestCase
{
    /**
    * @dataProvider providerGetController
    */
    public function testGetController($server, $class)
    {
        $service = $this
            ->getMockBuilder('\Api\Service')
            ->setMockClassName('Service')
            ->setConstructorArgs([$server,[],[]])
            ->getMock();
        
        $controller = ControllerFactory::getController($service);
        
        $this->assertInstanceOf($class, $controller);
    }
    public function providerGetController()
    {
        return [
            [
                ['REQUEST_METHOD' => 'POST','DOCUMENT_URI' => '/topic'],
                'Api\Controller\TopicController',
            ],
            [
                ['REQUEST_METHOD' => 'POST','DOCUMENT_URI' => '/topic/1/comment'],
                'Api\Controller\TopicController',
            ],
            [
                ['REQUEST_METHOD' => 'GET','DOCUMENT_URI' => '/topic/3'],
                'Api\Controller\TopicController',
            ],
            [
                ['REQUEST_METHOD' => 'GET','DOCUMENT_URI' => '/topics'],
                'Api\Controller\TopicController',
            ],
            [
                ['REQUEST_METHOD' => 'DELETE','DOCUMENT_URI' => '/topic/2'],
                'Api\Controller\TopicController',
            ],
            [
                ['REQUEST_METHOD' => 'GET','DOCUMENT_URI' => '/adasd'],
                'Api\Controller\ErrorController',
            ],
        ];
    }
}
