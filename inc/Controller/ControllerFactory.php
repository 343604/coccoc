<?php
namespace Api\Controller;

use Api\Service;
use Api\Http\Request;
use Api\Http\Response;

/**
 * Controller Factory
 */
class ControllerFactory
{
    /**
     * Find controller by appropriate query or return ErrorController
     *
     * @return Controller
     */
    public static function getController(Service $service)
    {
        $methods = [
            'POST' => [
                ['topic',                 'Topic',   'Create'],
                ['topic/(\d+)/comment',   'Topic',   'AddComment'],
            ],
            'GET' => [
                ['topic/(\d+)',           'Topic',   'GetById'],
                ['topics',                'Topic',   'GetList'],
            ],
            'DELETE' => [
                ['topic/(\d+)',           'Topic',   'Delete'],
            ],
        ];
        
        $elm = null;
        if (isset($methods[$service->request->server['REQUEST_METHOD']])) {
            foreach ($methods[$service->request->server['REQUEST_METHOD']] as $v) {
                if (preg_match('#^/'.$v[0].'$#', $service->request->server['DOCUMENT_URI'], $matches)) {
                    $elm = $v;
                    $service->request->params = array_slice($matches, 1);
                    break;
                }
            }
            if ($elm) {
                list($query, $entityName, $method) = $elm;
            }
        }
        if (!$elm) {
            $entityName = 'Error';
            $method = 'E404';
        }
        
        $service->request->action = 'action' . $method;
        
        $classRepository = 'Api\\Repository\\' . $entityName . 'Repository';
        $repository = new $classRepository($service);
        
        $classHydrator = 'Api\\RequestHydrator\\' . $entityName . 'RequestHydrator';
        $hydrator = new $classHydrator($service);
        
        $classController = 'Api\\Controller\\' . $entityName . 'Controller';
        $controller = new $classController($service, $repository, $hydrator);
        
        return $controller;
    }
}
