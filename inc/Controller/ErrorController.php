<?php
namespace Api\Controller;

use Api\Service;
use Api\Http\Request;
use Api\Http\Response;
use Api\Repository\ErrorRepository;
use Api\RequestHydrator\ErrorRequestHydrator;

/**
 * Error Controller
 * Maker error responses
 */
class ErrorController
{
    /**
     * @var Service
     */
    protected $service;
    /**
     * @var ErrorRepository
     */
    protected $errorRepository;
    /**
     * @var ErrorRequestHydrator
     */
    protected $errorRequestHydrator;
  
    /**
     * Set executable method and arguments
     *
     * @param Service
     * @param ErrorRepository
     * @param ErrorRequestHydrator
     *
     * @return void
     */
    public function __construct(Service $service, ErrorRepository $errorRepository, ErrorRequestHydrator $errorRequestHydrator)
    {
        $this->service = $service;
        $this->errorRepository = $errorRepository;
        $this->errorRequestHydrator = $errorRequestHydrator;
    }
  
    /**
     * Http 404 response
     *
     * @param Request;
     * @param Response;
     *
     * @return Api\Http\Response
     */
    public function actionE404(Request $request, Response $response) : Response
    {
        $response->setStatusCode(404);
        return $response;
    }
}
