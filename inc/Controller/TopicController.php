<?php
namespace Api\Controller;

use Api\Service;
use Api\Http\Request;
use Api\Http\Response;
use Api\Model\User;
use Api\Model\Topic;
use Api\Model\Comment;
use Api\Repository\TopicRepository;
use Api\RequestHydrator\TopicRequestHydrator;
use Api\RequestHydrator\InvalidDataException;
use Api\RequestHydrator\AccessDenyException;
use Api\RequestHydrator\NotAuthorizedException;
use Api\RequestHydrator\NotFoundException;

/**
 * Topic Controller
 * Creating, adding comments, finding by id, list making, deleting
 */
class TopicController
{
    /**
     * @var Service
     */
    private $service;
    /**
     * @var TopicRepository
     */
    private $topicRepository;
    /**
     * @var TopicRequestHydrator
     */
    private $topicRequestHydrator;
  
    /**
     * Set executable method and arguments
     *
     * @param Service
     * @param TopicRepository
     * @param TopicRequestHydrator
     *
     * @return void
     */
    public function __construct(Service $service, TopicRepository $topicRepository, TopicRequestHydrator $topicRequestHydrator)
    {
        $this->service = $service;
        $this->topicRepository = $topicRepository;
        $this->topicRequestHydrator = $topicRequestHydrator;
    }
  
    /**
     * Creating Topic with check authorization
     *
     * @param Request;
     * @param Response;
     *
     * @return Api\Http\JsonResponse
     *    401 - not authorized
     *    400 - incorrect input params
     *    200 - ok
     */
    public function actionCreate(Request $request, Response $response) : Response
    {
        try {
            $newTopic = $this->topicRequestHydrator->hydrateCreate($request);
            $this->topicRepository->save($newTopic);

            return $response
                ->setStatusCode(200)
                ->setJsonBody(['id' => $newTopic->getId()]);
        } catch (InvalidDataException $e) {
            return $response
                ->setStatusCode(400)
                ->setJsonBody($e->getData());
        } catch (NotAuthorizedException $e) {
            return $response
                ->setStatusCode(401)
                ->setJsonBody([]);
        }
    }
  
    /**
     * Adding comment for topic
     *
     * @param Api\Http\Request;
     * @param integer id;
     *
     * @return Api\Http\JsonResponse
     *    404 - topic not found
     *    400 - incorrect input params
     *    200 - ok
     */
    public function actionAddComment(Request $request, Response $response) : Response
    {
        try {
            $newComment = $this->topicRequestHydrator->hydrateAddComment($request);
            $this->service->getRepository('Comment')->save($newComment);

            return $response
                ->setStatusCode(200)
                ->setJsonBody(['id' => $newComment->getId()]);
        } catch (InvalidDataException $e) {
            return $response
                ->setStatusCode(400)
                ->setJsonBody($e->getData());
        } catch (NotFoundException $e) {
            return $response
                ->setStatusCode(404)
                ->setJsonBody([]);
        }
    }
  
    /**
     * Finding by id
     *
     * @param Api\Http\Request;
     * @param integer id;
     *
     * @return Api\Http\JsonResponse
     *    404 - topic not found
     *    200 - ok
     */
    public function actionGetById(Request $request, Response $response) : Response
    {
        $topic = $this->topicRepository->findById($request->params[0]);
        if (!$topic) {
            return $response
                ->setStatusCode(404)
                ->setJsonBody([]);
        }
    
        return $response
            ->setStatusCode(200)
            ->setJsonBody($topic->toArray());
    }
  
    /**
     * Getting full list of topics
     *
     * @param Api\Http\Request;
     *
     * @return Api\Http\JsonResponse
     *    200 - ok
     */
    public function actionGetList(Request $request, Response $response) : Response
    {
        $list = $this->topicRepository->getList();
    
        return $response
            ->setStatusCode(200)
            ->setJsonBody($list);
    }
  
    /**
     * Deleting Topic with check authorization
     *
     * @param Api\Http\Request;
     * @param integer id;
     *
     * @return Api\Http\JsonResponse
     *    400 - incorrect input params
     *    401 - not authorized
     *    403 - owned by to another user
     *    404 - topic not found
     *    200 - ok
     */
    public function actionDelete(Request $request, Response $response) : Response
    {
        try {
            $topic = $this->topicRequestHydrator->hydrateDelete($request);
            $this->topicRepository->delete($topic);

            return $response
                ->setStatusCode(200)
                ->setJsonBody([]);
        } catch (NotAuthorizedException $e) {
            return $response
                ->setStatusCode(401)
                ->setJsonBody([]);
        } catch (AccessDenyException $e) {
            return $response
                ->setStatusCode(403)
                ->setJsonBody([]);
        } catch (NotFoundException $e) {
            return $response
                ->setStatusCode(404)
                ->setJsonBody([]);
        }
    }
}
