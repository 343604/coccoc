<?php
namespace Api\Model;

/**
 * Abstract Model
 */
abstract class Model
{
    protected $id;
    
    /**
     * Set id
     *
     * @param integer $id
     *
     * @return Object
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
  
    /**
     * Get all properties
     */
    public function toArray()
    {
        $properties = get_object_vars($this);
        foreach ($properties as &$property) {
            if (is_object($property)) {
                $property = $property->toArray();
            }
            if (is_array($property)) {
                foreach ($property as &$v) {
                    $v = $v->toArray();
                }
            }
        }
        return  $properties;
    }
}
