<?php
namespace Api\Model;

use Api\Db\Db;
use Api\Model\Model;
use Api\Model\Comment;

class Topic extends Model
{
    protected $subject;
    protected $text;
    protected $userId;
    protected $ts;
    protected $comments;

    /**
     * Set subject
     *
     * @param string $subject
     * @return Topic
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
        return $this;
    }
    /**
     * Get subject
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return Topic
     */
    public function setText($text)
    {
        $this->text = $text;
        return $this;
    }
    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     * @return Topic
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
        return $this;
    }
    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }
  
    /**
     * Get ts(timestamp)
     *
     * @return string
     */
    public function getTs()
    {
        return $this->ts;
    }

    /**
     * Set comments
     *
     * @param array $comments
     * @return Topic
     */
    public function setComments($comments)
    {
        $this->comments = $comments;
        return $this;
    }
    /**
     * Get comments
     *
     * @return array
     */
    public function getComments()
    {
        return $this->comments;
    }
}
