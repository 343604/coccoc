<?php
namespace Api\Model;

use Api\Db\Db;

/**
 * Topic comment model
 */
class Comment extends Model
{
    protected $text;
    protected $topicId;
    protected $ts;

    /**
     * Set text
     *
     * @param string $text
     * @return Comment
     */
    public function setText($text)
    {
        $this->text = substr($text, 0, 64000);
        return $this;
    }
    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set TopicId
     *
     * @param string $topicId
     * @return Comment
     */
    public function setTopicId($topicId)
    {
        $this->topicId = $topicId;
        return $this;
    }
    /**
     * Get TopicId
     *
     * @return integer
     */
    public function getTopicId()
    {
        return $this->topicId;
    }
  
    /**
     * Get ts(timestamp)
     *
     * @return string
     */
    public function getTs()
    {
        return $this->ts;
    }
}
