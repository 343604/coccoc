<?php
namespace Api\Model;

use Api\Db\Db;
use Api\Model\Model;
use Api\Repository\UserRepository;

/**
 * User model
 */
class User extends Model
{
    protected $token;

    /**
     * Set token
     *
     * @param string $token
     * @return User
     */
    public function setToken($token)
    {
        $this->token = $token;
        return $this;
    }
    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }
}
