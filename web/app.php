<?php
use Api\Service;

/**
 * @var Composer\Autoload\ClassLoader
 */
$loader = require '../vendor/autoload.php';

$Service = new Service($_SERVER, $_GET, $_POST);

$Service->run();
